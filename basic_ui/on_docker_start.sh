# this starts the UI on the docker container
#cd /root/adapt_ros_ws/run_scripts
xname=xaviertx

source /opt/ros/melodic/setup.bash
source ../devel/setup.bash
export ROS_MASTER_URI="http://${xname}:11311/"
roslaunch --wait wxpython_gui system_control_panel.launch
