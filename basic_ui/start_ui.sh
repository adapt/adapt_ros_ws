# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

container_name=adapt_basicui

# Location of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/utilities.sh

# Open up xhost (for display) only to the container.
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $container_name`

# If the container does not exist, start it.
echo "Starting ADAPT UI container"

#HOST_UID=$(echo $UID)
#HOST_GID=$(id -g)

# Maping /tmp/.X11-unix allows graphics to be passed.
docker run --rm -it \
  --network="host" \
  -e "DISPLAY" \
  -e "QT_X11_NO_MITSHM=1" \
  -v "/tmp/.X11-unix:/tmp/.X11-unix" \
  -v "$HOME/.config:$HOME/.config" \
  $container_name

remove_container $container_name
