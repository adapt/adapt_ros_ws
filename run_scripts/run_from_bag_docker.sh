#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
WSDIR="$SCRIPTDIR/../"

TMUX_SESSION=run_system_from_bag

# Define location of the workspace within the docker container.
WSDIR_DOCKER="~/adapt/adapt_ros_ws"

# Specify the bag to replay.
BAGFNAME="${WSDIR_DOCKER}/data/2021-06-23-16-51-24.bag"
BAGFNAME="${WSDIR_DOCKER}/data/2021-11-04-13-50-24.bag"

tmux kill-session -t $TMUX_SESSION

# Command to place oneself into bash terminal in docker container.
terminal_in_docker="${WSDIR}/docker/terminal_in_docker.sh"

# Start tmux session
tmux new-session -d -s $TMUX_SESSION
#tmux source ./.tmux.conf

# Roscore
tmux select-window -t $TMUX_SESSION:0
tmux rename-window -t $TMUX_SESSION:0 'Roscore'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m
tmux send-keys "roscore" C-m

sleep 1

# Image driver
tmux new-window -t $TMUX_SESSION:1 -n 'Rosbag'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m
tmux send-keys "rosbag play ${BAGFNAME} --l" C-m

# Image View
tmux new-window -t $TMUX_SESSION:2 -n 'Image View'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m
#tmux send-keys "rosrun rqt_image_view rqt_image_view" C-m

# Segmentation
tmux new-window -t $TMUX_SESSION:3 -n 'Segmentation'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m
tmux send-keys "roslaunch --wait segmentation segment.launch" C-m

# GUI
tmux new-window -t $TMUX_SESSION:4 -n 'GUI'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m
tmux send-keys "roslaunch --wait wxpython_gui system_control_panel.launch" C-m

# ImageView
tmux new-window -t $TMUX_SESSION:5 -n 'Nexus'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m
tmux send-keys "roslaunch --wait nexus nexus.launch \
                   sys_namespace:=/ \
                   rgb_image_topic:=/image_raw/compressed \
                   rgb_analytics_image_topic:=/rgb/segmented" C-m

# Test
tmux new-window -t $TMUX_SESSION:6 -n 'Test'
tmux send-keys "source ${terminal_in_docker}" C-m
tmux send-keys "cd ${WSDIR_DOCKER} && source activate_ros.bash" C-m

tmux select-window -t $TMUX_SESSION:7

# Bring up the tmux session
tmux attach -t $TMUX_SESSION
