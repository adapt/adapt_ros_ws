#!/bin/bash
sudo systemctl stop gpsd
sudo systemctl disable gpsd
sudo systemctl stop gpsd.socket
sudo systemctl disable gpsd.socket
sleep 1
sudo killall gpsd
sleep 1
#sudo gpsd -D 5 -N -n -b /dev/gps /dev/pps0 # run this way for debug info
#sudo /usr/local/sbin/gpsd -n -b /dev/gps /dev/pps0
sudo /usr/sbin/gpsd -n -b /dev/gps /dev/pps0