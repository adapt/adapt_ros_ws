#!/bin/bash
# change the ethernet settings to make sure we get the best rates:
# jumbo frames (default MTU = 1500):
#sudo ifconfig eth0 mtu 4000
# increase max recv buffer size:
#sudo sysctl -w net.core.rmem_max=33554432  # default=212992
#sudo sysctl -w net.core.netdev_max_backlog=2000  # 1000
#sudo sysctl -w net.core.netdev_budget=600  # 300

echo 'reading from gps NMEA data...'
# read some of the gps device
check_gps="head /dev/gps"
output=$(eval "$check_gps")
echo "GPS info: $output"
if [[ "$output" == *"\$GP"* ]]; then
  echo "Successfully read GPS NMEA info!"
else
  echo "GPS is not sending NMEA info to /dev/gps - check udev rules"
  exit 1
fi

# check for the PPS signal coming in
check_pps="dmesg --ctime | grep capture | head -10"
output=$(eval "$check_pps")
echo "dmesg - should see PPS every second: $output"
if [[ "$output" == *"pps pps0: capture assert"* ]]; then
  echo "pps events coming in - read above and make sure it is only once per second"
else
  echo "Not seeing the 1PPS messages :("
  exit 1
fi

sudo systemctl stop systemd-timesyncd.service
sudo systemctl disable systemd-timesyncd.service

# start with the fan on (Xavier):
sudo /usr/sbin/nvpmodel -d cool

echo 'finished setup'