echo '!!!! NOTE: ROS node should not be connected !!!'
width=5472
height=3648
pf='BayerRG8'
camera=adaptcam
# low res mono for 23Hz
#gc_config "${camera}" 'Width=1024'
#gc_config "${camera}" 'Height=768'
#gc_config "${camera}" 'PixelFormat=Mono8'
# medium res
#gc_config "${camera}" 'Width=2736'
#gc_config "${camera}" 'Height=1824'
#gc_config "${camera}" 'PixelFormat=RGB8Packed'
# full res color ~= 4Hz
#gc_config "${camera}" 'Width=5472'
#gc_config "${camera}" 'Height=3648'
#gc_config "${camera}" 'PixelFormat=BayerRG8'
echo "width=${width}"
echo "height=${height}"
echo "pixelFormat=${pf}"
gc_config "${camera}" "PixelFormat=${pf}"
gc_config "${camera}" "Width=${width}"
gc_config "${camera}" "Height=${height}"
gc_config "${camera}" "AcquisitionFrameRateEnable=1"
gc_config "${camera}" "AcquisitionFrameRate=4"

gc_config "${camera}" "BalanceWhiteAutoProfile=Outdoor"

# this setting will speed up Hz but isn't always RW:
# ISP Off is only supported with uninterpolated formats (Mono and Bayer)
gc_config "${camera}" 'IspEnable=0'
# ptp settings
gc_config "${camera}" 'GevIEEE1588=0' # turn this off to reset it
sleep 2
gc_config "${camera}" 'GevIEEE1588Mode=SlaveOnly'
gc_config "${camera}" 'GevIEEE1588=1'

#trigger settings
gc_config "${camera}" 'TriggerMode=Off'
#gc_config "${camera}" 'TriggerMode=On'
#gc_config "${camera}" 'LineSelector=Line2'
#gc_config "${camera}" 'LineMode=Input'
#gc_config "${camera}" 'TriggerSelector=FrameStart'
#gc_config "${camera}" 'TriggerSource=Line2'
#gc_config "${camera}" 'TriggerDelay=400' #us
#gc_config "${camera}" 'TriggerActivation=RisingEdge'

# exposure control
# default is 100us
gc_config "${camera}" 'AutoExposureExposureTimeLowerLimit=233'
# default is 15000us
gc_config "${camera}" 'AutoExposureExposureTimeUpperLimit=1000'

echo 'completed'
