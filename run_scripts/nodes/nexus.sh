#!/bin/bash
# setup ros
source /opt/ros/melodic/setup.bash
# run the activation for ros workspace
__DIR__="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
WS_DIR=${__DIR__}/../../
activate_ros=${WS_DIR}"/activate_ros.bash"
source $activate_ros

# run the node
roslaunch --wait nexus nexus.launch sys_namespace:=/ rgb_image_topic:=/image_raw

