#!/bin/bash
# TODO - find out if the interfaces can be grabbed automagically?
# ethernet interface for communication
netiface=eth0
# check the ethernet interface exists
if [[ $(ip a show $netiface 2>&1 >/dev/null | grep -c 'does not exist') -ge 1 ]] ; then
    echo "ERROR: Device $netiface does not exist."
    echo "You need to update setup_camera_comms.sh to proceed."
    exit 1
fi

# TODO: can the camera sn be grabbed automagically?
# serial number of the camera
sn=21032954

# list available genicam devices
echo " "
echo "NOTE If you don't see your genicam device listed below;
echo "  1. Check hardware connections,"
echo "  2. or run this command to setup your NIC:"
echo "sudo ifconfig $netiface 169.254.0.1 netmask 255.255.0.0 \"
echo " broadcast 169.254.0.255 mtu 9000"
echo "  3. See if you need to update the Serial Number in the setup_camera_comms.sh,"
echo "    SN is currently set to: $sn"

# check for the serial number
echo "Checking serial number..."
if [[ $(gc_config -l | grep -c "$sn") -eq 0 ]] ; then
    echo " "
    echo "ERROR: Genicam device with Serial number $sn was not found."
    echo "Look at the above or update setup_camera_comms.sh to proceed."
    exit 1
else
    echo "Genicam $sn was found.  Naming it adaptcam..."
fi

# set the name of the device for easy use
gc_config "${netiface}:${sn}" -n adaptcam

#force ptp on - this seems to cause issues - use the ros methiod
#gc_config adaptcam GevIEEE1588=1
#gc_config adaptcam 'GevIEEE1588Mode=SlaveOnly'
