#!/bin/bash

# check for device existance: gps, spatial, pps
if [[ $(ls /dev/ | grep 'pps') = 'pps0' ]]; then
  printf "\xE2\x9C\x94 Found /dev/pps0!\n"
else
  echo "ERROR: no pps in /dev/"
  exit 1
fi
if [[ $(ls /dev/ | grep 'gps') = 'gps' ]]; then
  printf "\xE2\x9C\x94 Found /dev/gps!\n"
else
  echo "ERROR: no gps in /dev/"
  exit 1
fi
if [[ $(ls /dev/ | grep 'spatial') = 'spatial' ]]; then
  printf "\xE2\x9C\x94 Found /dev/spatial!\n"
else
  echo "ERROR: no spatial in /dev/"
  exit 1
fi

# check for the PPS signal coming in
check_pps="dmesg --ctime | grep capture | head -5"
output=$(eval "$check_pps")
echo "dmesg - should see PPS every second: \n$output"
if [[ "$output" == *"pps pps0: capture assert"* ]]; then
  printf "\xE2\x9C\x94 pps events coming in - read above and make sure it is only once per second\n"
else
  echo "ERROR:Not seeing the 1PPS messages :("
fi

# check to make sure the gpsd service is running
cmd="ps ax | grep gpsd"
output=$(eval "$cmd")
if [[ "$output" == *"gpsd -n -b /dev/gps /dev/pps0"* ]]; then
  printf "\xE2\x9C\x94 gpsd is running!\n"
else
  echo "ERROR:It looks like gpsd is NOT running"
fi

# check to make sure the time service is running
#cmd="ps ax | grep ntp"
cmd="ps ax | grep chronyd"
output=$(eval "$cmd")
#if [[ "$output" == *"/usr/sbin/ntpd"* ]]; then
if [[ "$output" == *"/usr/sbin/chronyd"* ]]; then
  printf "\xE2\x9C\x94 chronyd is running!\n"
  #printf "\xE2\x9C\x94 ntpd is running!\n"
else
  echo "ERROR:It looks like chronyd is NOT running"
  #echo "ERROR:It looks like ntpd is NOT running"
fi

# check to make sure the ptp4l service is running
cmd="ps ax | grep ptp4l"
output=$(eval "$cmd")
if [[ "$output" == *"/usr/sbin/ptp4l"* ]]; then
  printf "\xE2\x9C\x94 ptp4l is running!\n"
else
  echo "ERROR:It looks like ptp4l is NOT running"
fi

# look for ptp0
if [[ $(ls /dev/ | grep 'ptp') = 'ptp0' ]]; then
  printf "\xE2\x9C\x94 Found /dev/ptp0!\n"
else
  echo "ERROR: no ptp in /dev/"
  exit 1
fi

# look for ptp1
if [[ $(ls /dev/ | grep 'ptp') = 'ptp1' ]]; then
  printf "Error: Found /dev/ptp1 - you may need to modify the ptp statup or reboot\n"
fi

echo 'run cgps to make sure the device is getting NMEA data'

