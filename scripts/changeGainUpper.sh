source /home/adapt/adapt_ros_ws/devel/setup.bash
echo "setting Upper gain to $1"
rosservice call /rc_genicam_camera/set_genicam_parameter "AutoExposureGainUpperLimit=$1"
