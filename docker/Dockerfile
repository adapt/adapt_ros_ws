# From Nvidia-supplied tensorrt 7.0.0 image
FROM nvcr.io/nvidia/tensorrt:20.03-py2 as base_cuda_ubuntu

WORKDIR /root
# setup environment
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV ROS_DISTRO melodic
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -q -y \
    dirmngr \
    gnupg2 \
    && rm -rf /var/lib/apt/lists/*

# setup keys
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu bionic main" > /etc/apt/sources.list.d/ros-latest.list

RUN apt-get update && apt-get install -q -y --allow-unauthenticated \
    python-catkin-tools \
    python-pip \
    python-tk \
    python-rosdep \
    python-rosinstall \
    python-vcstools

# bootstrap rosdep
RUN rosdep init && \
    rosdep update --rosdistro $ROS_DISTRO

# install ros packages up to perception
RUN apt-get update && apt-get install -y \
    ros-melodic-ros-core=1.4.1-0* \
    ros-melodic-ros-base=1.4.1-0* \
    ros-melodic-perception=1.4.1-0* \
    ros-melodic-rqt-image-view \
    libgl1-mesa-glx \
    libqt5x11extras5 \
    gdal-bin \
    python-gdal \
    ros-melodic-rospy-message-converter \
    && rm -rf /var/lib/apt/lists/*

# -----------------------------------------------------------------------------
# Additional installs so that Spyder can connect to the remote kernel and can
# view Matplotlib results.
RUN pip install \
    anaconda \
    ipython \
    ipykernel \
    matplotlib \
    spyder-kernels
# -----------------------------------------------------------------------------

RUN pip install --no-cache-dir \
    PyGeodesy \
    shapely \
    pyshp \
    simplekml \
    exifread==2.3.2 \
    ujson==1.35 \
    torch==1.4.0 \
    torchvision==0.5.0 \
    future \
    yacs \
    scipy \
    psutil \
    colour


# ============================= Install torch2trt ==================
RUN git clone https://github.com/saulzar/torch2trt \
    && cd torch2trt \
    && python setup.py install \
    && cd ../ \
    && rm -r ./torch2trt

RUN pip install Pillow --upgrade

CMD ["/bin/bash"]

