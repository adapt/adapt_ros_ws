#! /usr/bin/python
"""
check the system to confirm various services are running
"""
from __future__ import division, print_function
import os
from diagnostic_msgs.msg import DiagnosticStatus
import psutil


class checkHardware():
    """
    checks for the presence of PPS device + messages
    """

    def checkPPS(self, ppsdev, pub):
        ds = DiagnosticStatus()
        msg = ''
        exists = self.existanceCheck(ppsdev, pub, ds, 'PPS')
        if exists is False:
            return  # error - no need to continue
        # build a message to publish
        msg = 'Device ' + ppsdev + ' is available.'
        ds.level = 0  # OK

        # TODO: read dmesg to see if we see the signals once per second?

        ds.message = msg
        pub.publish(ds)
        return

    """
    checks for the presence of NMEA device + messages
    """

    def checkNMEA(self, nmeadev, pub, service):
        ds = DiagnosticStatus()
        msg = ''
        exists = self.existanceCheck(nmeadev, pub, ds, 'NMEA')
        if exists is False:
            return  # error - no need to continue

        msg = 'Device ' + nmeadev + ' is available.'
        # check for the service
        if self.serviceRunningCheck(service, ds,
                                    pub, msg) is False:
            return
        # TODO: other checks?
        return

    """
    checks for the presence of INS device + messages
    """

    def checkINS(self, insdev, pub, service):
        ds = DiagnosticStatus()
        msg = ''
        exists = self.existanceCheck(insdev, pub, ds, 'INS')
        if exists is False:
            return  # error - no need to continue
        msg = 'Device ' + insdev + ' is available.'

        # check for the service
        if self.serviceRunningCheck(service, ds,
                                    pub, msg) is False:
            return
        # TODO: other checks?
        return

    """
    checks for the presence of PTP device + messages
    """

    def checkPTP(self, ptpdev, pub, service):
        ds = DiagnosticStatus()
        msg = ''
        exists = self.existanceCheck(ptpdev, pub, ds, 'PTP')
        if exists is False:
            return  # error - no need to continue
        msg = 'Device ' + ptpdev + ' is available.'

        # check for the service
        if self.serviceRunningCheck(service, ds,
                                    pub, msg) is False:
            return
        # TODO: other checks?s
        return

    """
    sets up the DiagnosticStatus for the device and sees if it exists
    if not it publishes a message and returns false
    """

    def existanceCheck(self, dev, pub, ds, name):
        # setup device info:
        ds.name = name + ' device'
        ds.hardware_id = dev

        # check for the device
        if not self.devExists(dev):  # no device - report error
            ds.level = 2  # ERROR
            ds.message = name + ' device ' + dev + ' is unavailable.'
            pub.publish(ds)
            return False  # indicate the failure

        return True

        """
        checks for a system process running under this name
        """
    def serviceRunningCheck(self, service, ds, pub, msg):
        # check to see if the service is running
        running = service in (p.name() for p in psutil.process_iter())
        if running:  # check for the service
            msg += ' And the service ' + service + ' is running.'
            ds.level = 0  # OK
        else:
            msg += ' However, the service ' + service + ' is NOT running.'
            ds.level = 2
        ds.message = msg

        pub.publish(ds)
        return running

    def devExists(self, path):
        try:
            return os.path.exists(path)
        except Exception:
            return False
