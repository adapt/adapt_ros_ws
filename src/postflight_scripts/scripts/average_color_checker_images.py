#!/usr/bin/env python
from __future__ import division, print_function
import os
import numpy as np
import cv2
import PIL
import glob
import matplotlib.pyplot as plt

# ADAPT imports
from postflight_scripts.image_processing import srgb_gamma_decode_lut, \
    srgb_gamma_encode_lut


image_glob = '/host_filesystem/mnt/homenas2/noaa_adapt/adapt_flights/2022-03-30/images/colorchecker2/*.jpg'
out_fname = '/host_filesystem/mnt/homenas2/noaa_adapt/adapt_flights/2022-03-30/images/colorchecker2.jpg'


fnames = glob.glob(image_glob)

n = 0
for i, fname in enumerate(fnames):
    print('Processing %i/%i' % (i + 1, len(fnames)))
    img = cv2.imread(fname)[:, :, ::-1]
    img = srgb_gamma_decode_lut(img)

    if n == 0:
        image = img
    else:
        image += img

    n += 1

image /= np.percentile(image.ravel(), 99.8)
#image = np.round(image).astype(np.uint8)
#image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)

img2 = srgb_gamma_encode_lut(image)

img = cv2.imwrite(out_fname, img2[:, :, ::-1])
