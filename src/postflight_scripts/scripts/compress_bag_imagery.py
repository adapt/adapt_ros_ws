#!/usr/bin/env python
from __future__ import division, print_function
import os
import numpy as np
import cv2
import StringIO
from PIL import Image as PILImage

# ROS imports
from rosbag import Bag
from sensor_msgs.msg import CompressedImage
from cv_bridge import CvBridge


bridge = CvBridge()


bag_in_fname = '/home/user/adapt_ws/data/2021-11-04-13-50-24.bag'
bag_out_fname = '/home/user/adapt_ws/data/2021-11-04-13-50-24.compressed.bag'
imagery_in_topic = '/image_raw'
imagery_out_topic = '/image_raw/compressed'


with Bag(bag_in_fname, 'r') as ib:
    with Bag(bag_out_fname, 'w') as ob:
        for topic, msg, t in ib:
            if topic == imagery_in_topic:
                image = bridge.imgmsg_to_cv2(msg, "rgb8")
                
                compressed_msg = CompressedImage()
                compressed_msg.format = "jpeg"
                params = (cv2.IMWRITE_JPEG_QUALITY, 95)
                data = np.array(cv2.imencode('.jpg', image)[1]).tostring()
                compressed_msg.data = data
                compressed_msg.header = msg.header
                ob.write(topic, compressed_msg, compressed_msg.header.stamp)
            else:
                ob.write(topic, msg, msg.header.stamp if msg._has_header else t)