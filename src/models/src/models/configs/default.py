from yacs.config import CfgNode as CN

_C = CN()

# Distributed Options
_C.SYSTEM = CN()
_C.SYSTEM.NUM_GPUS = 1
_C.SYSTEM.NUM_WORKERS = 4

# Model Options
_C.MODEL = CN()
_C.MODEL.MODEL_NAME = 'bisenetv2'
_C.MODEL.NUM_CLASSES = 5
_C.MODEL.TRAIN_PATH = '.'
_C.MODEL.TRAIN_PREFIX = 'NOAA_ADAPT_ICE'

# Dataset Options
_C.DATASET = CN()
_C.DATASET.PATH = '.'

# Input processing
_C.INPUT = CN()
_C.INPUT.MEAN = (0.406, 0.456, 0.485)
_C.INPUT.STD  = (0.225, 0.224, 0.229)

# Training Options
_C.TRAIN = CN()
_C.TRAIN.BATCH_SIZE = 16
_C.TRAIN.INPUT_HEIGHT = 512
_C.TRAIN.INPUT_WIDTH = 1024

cfg = _C
