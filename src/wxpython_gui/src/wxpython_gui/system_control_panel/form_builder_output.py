# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Sep 16 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"System Control Panel", pos = wx.DefaultPosition, size = wx.Size( 1117,1147 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 400,400 ), wx.DefaultSize )
		self.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, wx.EmptyString ) )
		self.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		main_size = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer20 = wx.BoxSizer( wx.VERTICAL )
		
		self.ins_control_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bSizer191 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText142 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Navigation Data", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText142.Wrap( -1 )
		self.m_staticText142.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer191.Add( self.m_staticText142, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_staticline4 = wx.StaticLine( self.ins_control_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer191.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )
		
		bSizer181 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText181 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Lat (deg)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText181.Wrap( -1 )
		self.m_staticText181.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer181.Add( self.m_staticText181, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.lat_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 100,-1 ), wx.TE_CENTRE|wx.TE_READONLY )
		bSizer181.Add( self.lat_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		self.m_staticText25 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )
		bSizer181.Add( self.m_staticText25, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.easting_std_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer181.Add( self.easting_std_txt_ctrl, 0, wx.RIGHT|wx.LEFT, 5 )
		
		self.m_staticText251 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"m", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText251.Wrap( -1 )
		bSizer181.Add( self.m_staticText251, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer181, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )
		
		bSizer1811 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Lon (deg)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText.Wrap( -1 )
		self.m_staticText.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1811.Add( self.m_staticText, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.lon_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer1811.Add( self.lon_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		self.m_staticText252 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText252.Wrap( -1 )
		bSizer1811.Add( self.m_staticText252, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.northing_std_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer1811.Add( self.northing_std_txt_ctrl, 0, wx.RIGHT|wx.LEFT, 5 )
		
		self.m_staticText2511 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"m", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2511.Wrap( -1 )
		bSizer1811.Add( self.m_staticText2511, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer1811, 0, wx.EXPAND, 5 )
		
		bSizer1812 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText1812 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Alt HAE (m)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1812.Wrap( -1 )
		self.m_staticText1812.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1812.Add( self.m_staticText1812, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.alt_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer1812.Add( self.alt_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		self.m_staticText2521 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2521.Wrap( -1 )
		bSizer1812.Add( self.m_staticText2521, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.h_std_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer1812.Add( self.h_std_txt_ctrl, 0, wx.RIGHT|wx.LEFT, 5 )
		
		self.m_staticText25111 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"m", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25111.Wrap( -1 )
		bSizer1812.Add( self.m_staticText25111, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer1812, 0, wx.EXPAND, 5 )
		
		bSizer18121 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText18121 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Alt MSL (m)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18121.Wrap( -1 )
		self.m_staticText18121.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer18121.Add( self.m_staticText18121, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.alt_msl_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer18121.Add( self.alt_msl_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer18121, 1, wx.EXPAND, 5 )
		
		bSizer18191 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText18191 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Speed (m/s)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18191.Wrap( -1 )
		self.m_staticText18191.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer18191.Add( self.m_staticText18191, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.speed_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer18191.Add( self.speed_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer18191, 1, wx.EXPAND, 5 )
		
		bSizer1819 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText1819 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Heading (deg)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1819.Wrap( -1 )
		self.m_staticText1819.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1819.Add( self.m_staticText1819, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.heading_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer1819.Add( self.heading_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		self.m_staticText253 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText253.Wrap( -1 )
		bSizer1819.Add( self.m_staticText253, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.heading_std_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer1819.Add( self.heading_std_txt_ctrl, 0, wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText2512 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"°", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2512.Wrap( -1 )
		bSizer1819.Add( self.m_staticText2512, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer1819, 0, wx.EXPAND, 5 )
		
		bSizer1818 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText1818 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Pitch (deg)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1818.Wrap( -1 )
		self.m_staticText1818.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1818.Add( self.m_staticText1818, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.pitch_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer1818.Add( self.pitch_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		self.m_staticText254 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText254.Wrap( -1 )
		bSizer1818.Add( self.m_staticText254, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.pitch_std_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer1818.Add( self.pitch_std_txt_ctrl, 0, wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText25121 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"°", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25121.Wrap( -1 )
		bSizer1818.Add( self.m_staticText25121, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer191.Add( bSizer1818, 0, wx.EXPAND, 5 )
		
		bSizer1817 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText1817 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Roll (deg)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1817.Wrap( -1 )
		self.m_staticText1817.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1817.Add( self.m_staticText1817, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.roll_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer1817.Add( self.roll_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		self.m_staticText255 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"±", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText255.Wrap( -1 )
		bSizer1817.Add( self.m_staticText255, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.roll_std_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer1817.Add( self.roll_std_txt_ctrl, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_staticText25122 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"°", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25122.Wrap( -1 )
		bSizer1817.Add( self.m_staticText25122, 0, wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer191.Add( bSizer1817, 0, wx.EXPAND, 5 )
		
		bSizer1817131 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText1817131 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Time (s)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1817131.Wrap( -1 )
		self.m_staticText1817131.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer1817131.Add( self.m_staticText1817131, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.ins_time_txt_ctrl = wx.TextCtrl( self.ins_control_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CENTRE|wx.TE_READONLY )
		bSizer1817131.Add( self.ins_time_txt_ctrl, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5 )
		
		
		bSizer191.Add( bSizer1817131, 1, wx.EXPAND, 5 )
		
		
		bSizer191.AddSpacer( ( 0, 10), 0, 0, 5 )
		
		self.m_staticText1817131311 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"GNSS/INS Status", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1817131311.Wrap( -1 )
		self.m_staticText1817131311.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer191.Add( self.m_staticText1817131311, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		bSizer18171313 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer75 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer511 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer421 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText45 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Orientation Filter", wx.DefaultPosition, wx.Size( 150,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText45.Wrap( -1 )
		bSizer421.Add( self.m_staticText45, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.orientation_filter_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.orientation_filter_status_txt.Wrap( -1 )
		self.orientation_filter_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer421.Add( self.orientation_filter_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer511.Add( bSizer421, 0, 0, 5 )
		
		bSizer4211 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText451 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Navigation Filter ", wx.DefaultPosition, wx.Size( 150,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText451.Wrap( -1 )
		bSizer4211.Add( self.m_staticText451, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.navigation_filter_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.navigation_filter_status_txt.Wrap( -1 )
		self.navigation_filter_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer4211.Add( self.navigation_filter_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer511.Add( bSizer4211, 0, wx.EXPAND, 5 )
		
		bSizer4212 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText452 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Heading Initialised", wx.DefaultPosition, wx.Size( 150,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText452.Wrap( -1 )
		bSizer4212.Add( self.m_staticText452, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.heading_initialized_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.heading_initialized_status_txt.Wrap( -1 )
		self.heading_initialized_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer4212.Add( self.heading_initialized_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer511.Add( bSizer4212, 0, wx.EXPAND, 5 )
		
		bSizer42121 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText4521 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"UTC Time Initialised", wx.DefaultPosition, wx.Size( 150,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText4521.Wrap( -1 )
		bSizer42121.Add( self.m_staticText4521, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.utc_time_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.utc_time_status_txt.Wrap( -1 )
		self.utc_time_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer42121.Add( self.utc_time_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer511.Add( bSizer42121, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )
		
		
		bSizer75.Add( bSizer511, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		bSizer5111 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer4212112 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText452112 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Internal GNSS", wx.DefaultPosition, wx.Size( 140,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText452112.Wrap( -1 )
		bSizer4212112.Add( self.m_staticText452112, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.internal_gnss_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.internal_gnss_status_txt.Wrap( -1 )
		self.internal_gnss_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer4212112.Add( self.internal_gnss_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer5111.Add( bSizer4212112, 0, wx.EXPAND, 5 )
		
		bSizer42121113 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText4521113 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Magnetic Heading", wx.DefaultPosition, wx.Size( 140,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText4521113.Wrap( -1 )
		bSizer42121113.Add( self.m_staticText4521113, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.magnetic_heading_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.magnetic_heading_status_txt.Wrap( -1 )
		self.magnetic_heading_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer42121113.Add( self.magnetic_heading_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer5111.Add( bSizer42121113, 0, wx.EXPAND, 5 )
		
		bSizer421211111 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText45211111 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Velocity Heading Enabled", wx.DefaultPosition, wx.Size( 140,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText45211111.Wrap( -1 )
		bSizer421211111.Add( self.m_staticText45211111, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.velocity_heading_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.velocity_heading_status_txt.Wrap( -1 )
		self.velocity_heading_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer421211111.Add( self.velocity_heading_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer5111.Add( bSizer421211111, 0, wx.EXPAND, 5 )
		
		bSizer421211121 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText45211121 = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"Atmospheric Altitude", wx.DefaultPosition, wx.Size( 140,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText45211121.Wrap( -1 )
		bSizer421211121.Add( self.m_staticText45211121, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.atmospheric_altitude_status_txt = wx.StaticText( self.ins_control_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.atmospheric_altitude_status_txt.Wrap( -1 )
		self.atmospheric_altitude_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer421211121.Add( self.atmospheric_altitude_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer5111.Add( bSizer421211121, 0, wx.EXPAND, 5 )
		
		
		bSizer75.Add( bSizer5111, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer18171313.Add( bSizer75, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer191.Add( bSizer18171313, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.ins_control_panel.SetSizer( bSizer191 )
		self.ins_control_panel.Layout()
		bSizer191.Fit( self.ins_control_panel )
		bSizer20.Add( self.ins_control_panel, 1, wx.EXPAND|wx.BOTTOM, 5 )
		
		self.computer_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bSizer49 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText1421111 = wx.StaticText( self.computer_panel, wx.ID_ANY, u"Computer", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.m_staticText1421111.Wrap( -1 )
		self.m_staticText1421111.SetFont( wx.Font( 16, 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer49.Add( self.m_staticText1421111, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		bSizer551 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer56 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer421211 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText45211 = wx.StaticText( self.computer_panel, wx.ID_ANY, u"PTP - INS", wx.DefaultPosition, wx.Size( 100,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText45211.Wrap( -1 )
		bSizer421211.Add( self.m_staticText45211, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.ptp_ins_status_txt = wx.StaticText( self.computer_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ptp_ins_status_txt.Wrap( -1 )
		self.ptp_ins_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer421211.Add( self.ptp_ins_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer56.Add( bSizer421211, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		bSizer4212111 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText452111 = wx.StaticText( self.computer_panel, wx.ID_ANY, u"PTP - NMEA", wx.DefaultPosition, wx.Size( 100,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText452111.Wrap( -1 )
		bSizer4212111.Add( self.m_staticText452111, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.ptp_nmea_status_txt = wx.StaticText( self.computer_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ptp_nmea_status_txt.Wrap( -1 )
		self.ptp_nmea_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer4212111.Add( self.ptp_nmea_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer56.Add( bSizer4212111, 1, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		bSizer551.Add( bSizer56, 0, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		bSizer561 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer4212113 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText452113 = wx.StaticText( self.computer_panel, wx.ID_ANY, u"PTP - 1PPS", wx.DefaultPosition, wx.Size( 100,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText452113.Wrap( -1 )
		bSizer4212113.Add( self.m_staticText452113, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.ptp_1pps_status_txt = wx.StaticText( self.computer_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ptp_1pps_status_txt.Wrap( -1 )
		self.ptp_1pps_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer4212113.Add( self.ptp_1pps_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer561.Add( bSizer4212113, 0, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		bSizer42121111 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText4521111 = wx.StaticText( self.computer_panel, wx.ID_ANY, u"PTP Time Sync", wx.DefaultPosition, wx.Size( 100,-1 ), wx.ALIGN_RIGHT )
		self.m_staticText4521111.Wrap( -1 )
		bSizer42121111.Add( self.m_staticText4521111, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		self.ptp_status_txt = wx.StaticText( self.computer_panel, wx.ID_ANY, u"☐", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ptp_status_txt.Wrap( -1 )
		self.ptp_status_txt.SetFont( wx.Font( 16, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer42121111.Add( self.ptp_status_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )
		
		
		bSizer561.Add( bSizer42121111, 1, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		bSizer551.Add( bSizer561, 1, wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer49.Add( bSizer551, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.BOTTOM, 5 )
		
		
		self.computer_panel.SetSizer( bSizer49 )
		self.computer_panel.Layout()
		bSizer49.Fit( self.computer_panel )
		bSizer20.Add( self.computer_panel, 0, wx.EXPAND|wx.BOTTOM, 5 )
		
		self.camera_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		self.camera_panel.Enable( False )
		self.camera_panel.Hide()
		
		m_staticText14211 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText142111 = wx.StaticText( self.camera_panel, wx.ID_ANY, u"Camera Settings", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText142111.Wrap( -1 )
		self.m_staticText142111.SetFont( wx.Font( 16, 70, 90, 92, False, wx.EmptyString ) )
		
		m_staticText14211.Add( self.m_staticText142111, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		bSizer55 = wx.BoxSizer( wx.HORIZONTAL )
		
		
		bSizer55.AddSpacer( ( 10, 0), 0, wx.EXPAND, 5 )
		
		camera_setting_rgb_uv_comboChoices = [ u"Camera 1", u"Camera 2" ]
		self.camera_setting_rgb_uv_combo = wx.ComboBox( self.camera_panel, wx.ID_ANY, u"Camera 1", wx.DefaultPosition, wx.Size( -1,-1 ), camera_setting_rgb_uv_comboChoices, wx.CB_READONLY )
		self.camera_setting_rgb_uv_combo.SetSelection( 0 )
		bSizer55.Add( self.camera_setting_rgb_uv_combo, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer55.AddSpacer( ( 10, 0), 0, wx.EXPAND, 5 )
		
		
		m_staticText14211.Add( bSizer55, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )
		
		camera_setting_exposure_gainChoices = [ u"Exposure (ms)", u"Gain (0 - 32)" ]
		self.camera_setting_exposure_gain = wx.ComboBox( self.camera_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), camera_setting_exposure_gainChoices, wx.CB_READONLY )
		self.camera_setting_exposure_gain.SetSelection( 0 )
		m_staticText14211.Add( self.camera_setting_exposure_gain, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		bSizer442 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_button71 = wx.Button( self.camera_panel, wx.ID_ANY, u"Set Parameter", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button71.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer442.Add( self.m_button71, 0, wx.ALL, 5 )
		
		self.exposure_value_txt_ctrl = wx.TextCtrl( self.camera_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer442.Add( self.exposure_value_txt_ctrl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		
		m_staticText14211.Add( bSizer442, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.camera_panel.SetSizer( m_staticText14211 )
		self.camera_panel.Layout()
		m_staticText14211.Fit( self.camera_panel )
		bSizer20.Add( self.camera_panel, 0, wx.EXPAND|wx.BOTTOM, 5 )
		
		self.flight_data_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bSizer391 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText14211 = wx.StaticText( self.flight_data_panel, wx.ID_ANY, u"Data Collection", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText14211.Wrap( -1 )
		self.m_staticText14211.SetFont( wx.Font( 16, 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer391.Add( self.m_staticText14211, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.m_staticline1 = wx.StaticLine( self.flight_data_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		self.m_staticline1.Hide()
		
		bSizer391.Add( self.m_staticline1, 0, wx.EXPAND |wx.ALL, 5 )
		
		bSizer41 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer42 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer441 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText18171311 = wx.StaticText( self.flight_data_panel, wx.ID_ANY, u"Effort", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText18171311.Wrap( -1 )
		self.m_staticText18171311.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		self.m_staticText18171311.Hide()
		
		bSizer441.Add( self.m_staticText18171311, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		effort_combo_boxChoices = []
		self.effort_combo_box = wx.ComboBox( self.flight_data_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, effort_combo_boxChoices, 0 )
		self.effort_combo_box.Hide()
		
		bSizer441.Add( self.effort_combo_box, 1, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )
		
		
		bSizer42.Add( bSizer441, 1, wx.EXPAND, 5 )
		
		bSizer43 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_button4 = wx.Button( self.flight_data_panel, wx.ID_ANY, u"New", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button4.Hide()
		
		bSizer43.Add( self.m_button4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button5 = wx.Button( self.flight_data_panel, wx.ID_ANY, u"Edit", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button5.Hide()
		
		bSizer43.Add( self.m_button5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button6 = wx.Button( self.flight_data_panel, wx.ID_ANY, u"Delete", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_button6.Hide()
		
		bSizer43.Add( self.m_button6, 0, wx.ALL, 5 )
		
		
		bSizer42.Add( bSizer43, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticline41 = wx.StaticLine( self.flight_data_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		self.m_staticline41.Hide()
		
		bSizer42.Add( self.m_staticline41, 0, wx.EXPAND |wx.ALL, 5 )
		
		bSizer45 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText33 = wx.StaticText( self.flight_data_panel, wx.ID_ANY, u"Flight:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText33.Wrap( -1 )
		self.m_staticText33.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		self.m_staticText33.Hide()
		
		bSizer45.Add( self.m_staticText33, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText34 = wx.StaticText( self.flight_data_panel, wx.ID_ANY, u"FL", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText34.Wrap( -1 )
		self.m_staticText34.Hide()
		
		bSizer45.Add( self.m_staticText34, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )
		
		self.flight_number_text_ctrl = wx.TextCtrl( self.flight_data_panel, wx.ID_ANY, u"00", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.flight_number_text_ctrl.Hide()
		
		bSizer45.Add( self.flight_number_text_ctrl, 0, wx.TOP|wx.BOTTOM|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer42.Add( bSizer45, 1, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		bSizer41.Add( bSizer42, 1, wx.EXPAND, 5 )
		
		
		bSizer391.Add( bSizer41, 0, wx.EXPAND, 5 )
		
		bSizer401 = wx.BoxSizer( wx.HORIZONTAL )
		
		
		bSizer391.Add( bSizer401, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticline3 = wx.StaticLine( self.flight_data_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		self.m_staticline3.Hide()
		
		bSizer391.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )
		
		bSizer44 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.recording_gauge = wx.Gauge( self.flight_data_panel, wx.ID_ANY, 1, wx.DefaultPosition, wx.Size( 25,25 ), wx.GA_HORIZONTAL )
		self.recording_gauge.SetValue( 0 ) 
		bSizer44.Add( self.recording_gauge, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.start_collecting_button = wx.Button( self.flight_data_panel, wx.ID_ANY, u"Start Collecting", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer44.Add( self.start_collecting_button, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button8 = wx.Button( self.flight_data_panel, wx.ID_ANY, u"Stop Collecting", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer44.Add( self.m_button8, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer391.Add( bSizer44, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		bSizer443 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.reboot_system_button = wx.Button( self.flight_data_panel, wx.ID_ANY, u"Reboot System", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer443.Add( self.reboot_system_button, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.shutdown_system_button = wx.Button( self.flight_data_panel, wx.ID_ANY, u"Shutdown System", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer443.Add( self.shutdown_system_button, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer391.Add( bSizer443, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )
		
		
		self.flight_data_panel.SetSizer( bSizer391 )
		self.flight_data_panel.Layout()
		bSizer391.Fit( self.flight_data_panel )
		bSizer20.Add( self.flight_data_panel, 1, wx.EXPAND, 5 )
		
		self.m_panel7 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bSizer17 = wx.BoxSizer( wx.VERTICAL )
		
		self.close_button = wx.Button( self.m_panel7, wx.ID_ANY, u"Close", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		bSizer17.Add( self.close_button, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.m_panel7.SetSizer( bSizer17 )
		self.m_panel7.Layout()
		bSizer17.Fit( self.m_panel7 )
		bSizer20.Add( self.m_panel7, 0, wx.EXPAND|wx.TOP, 5 )
		
		
		main_size.Add( bSizer20, 0, wx.ALIGN_CENTER_VERTICAL|wx.EXPAND|wx.ALL, 5 )
		
		bsizer12 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel37 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bSizer59 = wx.BoxSizer( wx.VERTICAL )
		
		
		self.m_panel37.SetSizer( bSizer59 )
		self.m_panel37.Layout()
		bSizer59.Fit( self.m_panel37 )
		bsizer12.Add( self.m_panel37, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND|wx.TOP|wx.RIGHT, 5 )
		
		self.images_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.TAB_TRAVERSAL )
		self.images_panel.SetFont( wx.Font( 9, 70, 90, 90, False, wx.EmptyString ) )
		
		bSizer16 = wx.BoxSizer( wx.VERTICAL )
		
		top_row_bSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_panel_camera1 = wx.Panel( self.images_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		left_bsizer0 = wx.BoxSizer( wx.VERTICAL )
		
		self.camera1_image_title = wx.StaticText( self.m_panel_camera1, wx.ID_ANY, u"Camera 1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.camera1_image_title.Wrap( -1 )
		self.camera1_image_title.SetFont( wx.Font( 14, 74, 90, 92, False, "Sans" ) )
		
		left_bsizer0.Add( self.camera1_image_title, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.camera1_status_text = wx.StaticText( self.m_panel_camera1, wx.ID_ANY, u"Empty", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.camera1_status_text.Wrap( -1 )
		self.camera1_status_text.SetFont( wx.Font( 10, 70, 90, 92, False, wx.EmptyString ) )
		
		left_bsizer0.Add( self.camera1_status_text, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.camera1_panel = wx.Panel( self.m_panel_camera1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		left_bsizer0.Add( self.camera1_panel, 5, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.camera1_histogram_panel = wx.Panel( self.m_panel_camera1, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,50 ), wx.TAB_TRAVERSAL )
		left_bsizer0.Add( self.camera1_histogram_panel, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticline5 = wx.StaticLine( self.m_panel_camera1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		left_bsizer0.Add( self.m_staticline5, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.camera1_analytic_image_title = wx.StaticText( self.m_panel_camera1, wx.ID_ANY, u"Camera 1 Analytics", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.camera1_analytic_image_title.Wrap( -1 )
		self.camera1_analytic_image_title.SetFont( wx.Font( 14, 74, 90, 92, False, "Sans" ) )
		
		left_bsizer0.Add( self.camera1_analytic_image_title, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.camera1_analytics_status_text = wx.StaticText( self.m_panel_camera1, wx.ID_ANY, u"Empty", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.camera1_analytics_status_text.Wrap( -1 )
		self.camera1_analytics_status_text.SetFont( wx.Font( 10, 70, 90, 92, False, wx.EmptyString ) )
		
		left_bsizer0.Add( self.camera1_analytics_status_text, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.camera1_analytics_panel = wx.Panel( self.m_panel_camera1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		left_bsizer0.Add( self.camera1_analytics_panel, 5, wx.EXPAND |wx.ALL, 5 )
		
		
		self.m_panel_camera1.SetSizer( left_bsizer0 )
		self.m_panel_camera1.Layout()
		left_bsizer0.Fit( self.m_panel_camera1 )
		top_row_bSizer.Add( self.m_panel_camera1, 1, wx.EXPAND, 5 )
		
		self.m_panel_camera2 = wx.Panel( self.images_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		right_bsizer0 = wx.BoxSizer( wx.VERTICAL )
		
		self.camera2_image_title = wx.StaticText( self.m_panel_camera2, wx.ID_ANY, u"Camera 2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.camera2_image_title.Wrap( -1 )
		self.camera2_image_title.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		
		right_bsizer0.Add( self.camera2_image_title, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		bSizer32 = wx.BoxSizer( wx.VERTICAL )
		
		self.camera2_panel = wx.Panel( self.m_panel_camera2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer32.Add( self.camera2_panel, 5, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND|wx.ALL, 5 )
		
		self.camera2_histogram_panel = wx.Panel( self.m_panel_camera2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer32.Add( self.camera2_histogram_panel, 1, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		right_bsizer0.Add( bSizer32, 1, wx.EXPAND, 5 )
		
		self.center_rgb_status_text = wx.StaticText( self.m_panel_camera2, wx.ID_ANY, u"Empty", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.center_rgb_status_text.Wrap( -1 )
		self.center_rgb_status_text.SetFont( wx.Font( 10, 70, 90, 92, False, wx.EmptyString ) )
		
		right_bsizer0.Add( self.center_rgb_status_text, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.m_panel_camera2.SetSizer( right_bsizer0 )
		self.m_panel_camera2.Layout()
		right_bsizer0.Fit( self.m_panel_camera2 )
		top_row_bSizer.Add( self.m_panel_camera2, 1, wx.EXPAND, 5 )
		
		
		bSizer16.Add( top_row_bSizer, 1, wx.EXPAND, 5 )
		
		bottom_row_bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.sys1_disk_usage_panel = wx.Panel( self.images_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bsizer213 = wx.BoxSizer( wx.VERTICAL )
		
		self.left_det_static_text = wx.StaticText( self.sys1_disk_usage_panel, wx.ID_ANY, u"Detector", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.left_det_static_text.Wrap( -1 )
		self.left_det_static_text.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		
		bsizer213.Add( self.left_det_static_text, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.sys1_disk_usage_panel.SetSizer( bsizer213 )
		self.sys1_disk_usage_panel.Layout()
		bsizer213.Fit( self.sys1_disk_usage_panel )
		bottom_row_bSizer1.Add( self.sys1_disk_usage_panel, 1, 0, 5 )
		
		
		bSizer16.Add( bottom_row_bSizer1, 0, wx.EXPAND, 5 )
		
		bottom_row_bSizer11 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.disk_usage_panel = wx.Panel( self.images_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bsizer21221 = wx.BoxSizer( wx.VERTICAL )
		
		self.disk_usage_static_text = wx.StaticText( self.disk_usage_panel, wx.ID_ANY, u"Available Disk Space: ?", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.disk_usage_static_text.Wrap( -1 )
		self.disk_usage_static_text.SetFont( wx.Font( 14, 70, 90, 92, False, wx.EmptyString ) )
		
		bsizer21221.Add( self.disk_usage_static_text, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.disk_usage_panel.SetSizer( bsizer21221 )
		self.disk_usage_panel.Layout()
		bsizer21221.Fit( self.disk_usage_panel )
		bottom_row_bSizer11.Add( self.disk_usage_panel, 1, 0, 5 )
		
		
		bSizer16.Add( bottom_row_bSizer11, 0, wx.EXPAND, 5 )
		
		
		self.images_panel.SetSizer( bSizer16 )
		self.images_panel.Layout()
		bSizer16.Fit( self.images_panel )
		bsizer12.Add( self.images_panel, 1, wx.EXPAND|wx.BOTTOM|wx.RIGHT, 5 )
		
		
		main_size.Add( bsizer12, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( main_size )
		self.Layout()
		self.m_menubar1 = wx.MenuBar( 0 )
		self.exit_menu = wx.Menu()
		self.exit_menu_item = wx.MenuItem( self.exit_menu, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.exit_menu.AppendItem( self.exit_menu_item )
		
		self.m_menubar1.Append( self.exit_menu, u"File" ) 
		
		self.view_menu = wx.Menu()
		self.m_menuItem6 = wx.MenuItem( self.view_menu, wx.ID_ANY, u"Show/Hide Camera 1", wx.EmptyString, wx.ITEM_NORMAL )
		self.view_menu.AppendItem( self.m_menuItem6 )
		
		self.m_menuItem7 = wx.MenuItem( self.view_menu, wx.ID_ANY, u"Show/Hide Camera 2", wx.EmptyString, wx.ITEM_NORMAL )
		self.view_menu.AppendItem( self.m_menuItem7 )
		
		self.m_menuItem9 = wx.MenuItem( self.view_menu, wx.ID_ANY, u"Toggle Saturated Pixels", wx.EmptyString, wx.ITEM_NORMAL )
		self.view_menu.AppendItem( self.m_menuItem9 )
		
		self.m_menubar1.Append( self.view_menu, u"View" ) 
		
		self.m_detectors_menu = wx.Menu()
		self.m_menuItem1513 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Start Detectors", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem1513 )
		
		self.m_menuItem1514 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Stop Detectors", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem1514 )
		
		self.m_menuItem151 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Start Detector Sys0 (Center)", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem151 )
		
		self.m_menuItem1511 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Start Detector Sys1 (Left)", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem1511 )
		
		self.m_menuItem1512 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Start Detector Sys2 (Right)", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem1512 )
		
		self.m_menuItem15121 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Stop Detector Sys0 (Center)", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem15121 )
		
		self.m_menuItem151211 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Stop Detector Sys1 (Left)", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem151211 )
		
		self.m_menuItem151212 = wx.MenuItem( self.m_detectors_menu, wx.ID_ANY, u"Stop Detector Sys2 (Right)", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_detectors_menu.AppendItem( self.m_menuItem151212 )
		
		self.m_menubar1.Append( self.m_detectors_menu, u"Detection" ) 
		
		self.m_menu81 = wx.Menu()
		self.m_menuItem24 = wx.MenuItem( self.m_menu81, wx.ID_ANY, u"System Control Panel", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu81.AppendItem( self.m_menuItem24 )
		
		self.m_menubar1.Append( self.m_menu81, u"System-Control" ) 
		
		self.m_menu811 = wx.Menu()
		self.m_menuItem241 = wx.MenuItem( self.m_menu811, wx.ID_ANY, u"Open Map View", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu811.AppendItem( self.m_menuItem241 )
		
		self.m_menubar1.Append( self.m_menu811, u"Map" ) 
		
		self.m_menu8 = wx.Menu()
		self.m_menuItem281 = wx.MenuItem( self.m_menu8, wx.ID_ANY, u"Create Flight Summary", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu8.AppendItem( self.m_menuItem281 )
		
		self.m_menuItem29 = wx.MenuItem( self.m_menu8, wx.ID_ANY, u"Fine Tune Tracking", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu8.AppendItem( self.m_menuItem29 )
		
		self.m_menuItem31 = wx.MenuItem( self.m_menu8, wx.ID_ANY, u"Detection Summary", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu8.AppendItem( self.m_menuItem31 )
		
		self.m_menubar1.Append( self.m_menu8, u"Post-Flight Processing" ) 
		
		self.menu_help = wx.Menu()
		self.m_menuItem131 = wx.MenuItem( self.menu_help, wx.ID_ANY, u"Hot Keys", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_help.AppendItem( self.m_menuItem131 )
		
		self.menu_item_about = wx.MenuItem( self.menu_help, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_help.AppendItem( self.menu_item_about )
		
		self.m_menubar1.Append( self.menu_help, u"Help" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button71.Bind( wx.EVT_BUTTON, self.on_set_exposure )
		self.exposure_value_txt_ctrl.Bind( wx.EVT_TEXT_ENTER, self.on_set_exposure )
		self.effort_combo_box.Bind( wx.EVT_COMBOBOX, self.on_effort_selection )
		self.m_button4.Bind( wx.EVT_BUTTON, self.on_new_effort_metadata_entry )
		self.m_button5.Bind( wx.EVT_BUTTON, self.on_edit_effort_metadata )
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_delete_effort_metadata )
		self.flight_number_text_ctrl.Bind( wx.EVT_TEXT, self.on_update_flight_number )
		self.start_collecting_button.Bind( wx.EVT_BUTTON, self.start_collecting )
		self.m_button8.Bind( wx.EVT_BUTTON, self.stop_collecting )
		self.reboot_system_button.Bind( wx.EVT_BUTTON, self.reboot_system )
		self.shutdown_system_button.Bind( wx.EVT_BUTTON, self.shutdown_system )
		self.close_button.Bind( wx.EVT_BUTTON, self.on_close_button )
		self.m_panel_camera1.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_left_rgb )
		self.camera1_image_title.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_left_rgb )
		self.camera1_status_text.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_left_rgb )
		self.camera1_panel.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_camera1 )
		self.camera1_analytic_image_title.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_left_rgb )
		self.camera1_analytics_status_text.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_left_rgb )
		self.m_panel_camera2.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_center_rgb )
		self.camera2_image_title.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_center_rgb )
		self.camera2_panel.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_camera2 )
		self.camera2_histogram_panel.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_center_rgb )
		self.center_rgb_status_text.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_center_rgb )
		self.left_det_static_text.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_left_uv )
		self.disk_usage_static_text.Bind( wx.EVT_LEFT_DCLICK, self.on_dclick_center_uv )
		self.Bind( wx.EVT_MENU, self.on_close_button, id = self.exit_menu_item.GetId() )
		self.Bind( wx.EVT_MENU, self.on_show_or_hide_camera1, id = self.m_menuItem6.GetId() )
		self.Bind( wx.EVT_MENU, self.on_show_or_hide_camera2, id = self.m_menuItem7.GetId() )
		self.Bind( wx.EVT_MENU, self.on_toggle_saturated_pixels, id = self.m_menuItem9.GetId() )
		self.Bind( wx.EVT_MENU, self.on_start_detectors, id = self.m_menuItem1513.GetId() )
		self.Bind( wx.EVT_MENU, self.on_stop_detectors, id = self.m_menuItem1514.GetId() )
		self.Bind( wx.EVT_MENU, self.on_start_detector_sys0, id = self.m_menuItem151.GetId() )
		self.Bind( wx.EVT_MENU, self.on_start_detector_sys1, id = self.m_menuItem1511.GetId() )
		self.Bind( wx.EVT_MENU, self.on_start_detector_sys2, id = self.m_menuItem1512.GetId() )
		self.Bind( wx.EVT_MENU, self.on_stop_detector_sys0, id = self.m_menuItem15121.GetId() )
		self.Bind( wx.EVT_MENU, self.on_stop_detector_sys1, id = self.m_menuItem151211.GetId() )
		self.Bind( wx.EVT_MENU, self.on_stop_detector_sys2, id = self.m_menuItem151212.GetId() )
		self.Bind( wx.EVT_MENU, self.on_system_control_frame_raise, id = self.m_menuItem24.GetId() )
		self.Bind( wx.EVT_MENU, self.on_base_layer_frame_raise, id = self.m_menuItem241.GetId() )
		self.Bind( wx.EVT_MENU, self.on_create_flight_summary, id = self.m_menuItem281.GetId() )
		self.Bind( wx.EVT_MENU, self.on_measure_image_to_image_homographies, id = self.m_menuItem29.GetId() )
		self.Bind( wx.EVT_MENU, self.on_detection_summary, id = self.m_menuItem31.GetId() )
		self.Bind( wx.EVT_MENU, self.on_hot_key_help, id = self.m_menuItem131.GetId() )
		self.Bind( wx.EVT_MENU, self.on_menu_item_about, id = self.menu_item_about.GetId() )
	
	def __del__( self ):
		# Disconnect Events
		self.m_button71.Unbind( wx.EVT_BUTTON, None )
		self.exposure_value_txt_ctrl.Unbind( wx.EVT_TEXT_ENTER, None )
		self.effort_combo_box.Unbind( wx.EVT_COMBOBOX, None )
		self.m_button4.Unbind( wx.EVT_BUTTON, None )
		self.m_button5.Unbind( wx.EVT_BUTTON, None )
		self.m_button6.Unbind( wx.EVT_BUTTON, None )
		self.flight_number_text_ctrl.Unbind( wx.EVT_TEXT, None )
		self.start_collecting_button.Unbind( wx.EVT_BUTTON, None )
		self.m_button8.Unbind( wx.EVT_BUTTON, None )
		self.reboot_system_button.Unbind( wx.EVT_BUTTON, None )
		self.shutdown_system_button.Unbind( wx.EVT_BUTTON, None )
		self.close_button.Unbind( wx.EVT_BUTTON, None )
		self.m_panel_camera1.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera1_image_title.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera1_status_text.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera1_panel.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera1_analytic_image_title.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera1_analytics_status_text.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.m_panel_camera2.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera2_image_title.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera2_panel.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.camera2_histogram_panel.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.center_rgb_status_text.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.left_det_static_text.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.disk_usage_static_text.Unbind( wx.EVT_LEFT_DCLICK, None )
		self.Unbind( wx.EVT_MENU, id = self.exit_menu_item.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem6.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem7.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem9.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem1513.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem1514.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem151.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem1511.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem1512.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem15121.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem151211.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem151212.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem24.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem241.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem281.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem29.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem31.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.m_menuItem131.GetId() )
		self.Unbind( wx.EVT_MENU, id = self.menu_item_about.GetId() )
	
	
	# Virtual event handlers, overide them in your derived class
	def on_set_exposure( self, event ):
		event.Skip()
	
	
	def on_effort_selection( self, event ):
		event.Skip()
	
	def on_new_effort_metadata_entry( self, event ):
		event.Skip()
	
	def on_edit_effort_metadata( self, event ):
		event.Skip()
	
	def on_delete_effort_metadata( self, event ):
		event.Skip()
	
	def on_update_flight_number( self, event ):
		event.Skip()
	
	def start_collecting( self, event ):
		event.Skip()
	
	def stop_collecting( self, event ):
		event.Skip()
	
	def reboot_system( self, event ):
		event.Skip()
	
	def shutdown_system( self, event ):
		event.Skip()
	
	def on_close_button( self, event ):
		event.Skip()
	
	def on_dclick_left_rgb( self, event ):
		event.Skip()
	
	
	
	def on_dclick_camera1( self, event ):
		event.Skip()
	
	
	
	def on_dclick_center_rgb( self, event ):
		event.Skip()
	
	
	def on_dclick_camera2( self, event ):
		event.Skip()
	
	
	
	def on_dclick_left_uv( self, event ):
		event.Skip()
	
	def on_dclick_center_uv( self, event ):
		event.Skip()
	
	
	def on_show_or_hide_camera1( self, event ):
		event.Skip()
	
	def on_show_or_hide_camera2( self, event ):
		event.Skip()
	
	def on_toggle_saturated_pixels( self, event ):
		event.Skip()
	
	def on_start_detectors( self, event ):
		event.Skip()
	
	def on_stop_detectors( self, event ):
		event.Skip()
	
	def on_start_detector_sys0( self, event ):
		event.Skip()
	
	def on_start_detector_sys1( self, event ):
		event.Skip()
	
	def on_start_detector_sys2( self, event ):
		event.Skip()
	
	def on_stop_detector_sys0( self, event ):
		event.Skip()
	
	def on_stop_detector_sys1( self, event ):
		event.Skip()
	
	def on_stop_detector_sys2( self, event ):
		event.Skip()
	
	def on_system_control_frame_raise( self, event ):
		event.Skip()
	
	def on_base_layer_frame_raise( self, event ):
		event.Skip()
	
	def on_create_flight_summary( self, event ):
		event.Skip()
	
	def on_measure_image_to_image_homographies( self, event ):
		event.Skip()
	
	def on_detection_summary( self, event ):
		event.Skip()
	
	def on_hot_key_help( self, event ):
		event.Skip()
	
	def on_menu_item_about( self, event ):
		event.Skip()
	

