# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Sep 16 2021)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"System Startup and Control Commands", pos = wx.DefaultPosition, size = wx.Size( 893,701 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( -1,-1 ), wx.DefaultSize )
		self.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, wx.EmptyString ) )
		self.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		main_size = wx.BoxSizer( wx.HORIZONTAL )
		
		self.main_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.RAISED_BORDER|wx.TAB_TRAVERSAL )
		bSizer44 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer8 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText341 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Entire System", wx.DefaultPosition, wx.Size( 200,-1 ), wx.ALIGN_CENTRE )
		self.m_staticText341.Wrap( -1 )
		self.m_staticText341.SetFont( wx.Font( 18, 70, 90, 92, False, wx.EmptyString ) )
		self.m_staticText341.SetMinSize( wx.Size( -1,30 ) )
		
		bSizer8.Add( self.m_staticText341, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticline3 = wx.StaticLine( self.main_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer8.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )
		
		bSizer32 = wx.BoxSizer( wx.VERTICAL )
		
		self.camera_main_static_txt = wx.StaticText( self.main_panel, wx.ID_ANY, u"Camera", wx.DefaultPosition, wx.Size( 200,30 ), wx.ALIGN_CENTRE )
		self.camera_main_static_txt.Wrap( -1 )
		self.camera_main_static_txt.SetFont( wx.Font( 18, 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer32.Add( self.camera_main_static_txt, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText5 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Select Camera", wx.DefaultPosition, wx.Size( 140,-1 ), 0 )
		self.m_staticText5.Wrap( -1 )
		self.m_staticText5.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		bSizer6.Add( self.m_staticText5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		camera_select_comboChoices = []
		self.camera_select_combo = wx.ComboBox( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), camera_select_comboChoices, 0 )
		bSizer6.Add( self.camera_select_combo, 0, wx.ALL, 5 )
		
		
		bSizer32.Add( bSizer6, 0, 0, 5 )
		
		self.m_button14211 = wx.Button( self.main_panel, wx.ID_ANY, u"PTP Enable", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer32.Add( self.m_button14211, 0, wx.ALL, 5 )
		
		auto_expose_upper_limit_bsizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText8 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Auto Exposure Time\n     Upper Limit", wx.DefaultPosition, wx.Size( 180,-1 ), 0 )
		self.m_staticText8.Wrap( -1 )
		self.m_staticText8.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		auto_expose_upper_limit_bsizer.Add( self.m_staticText8, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.auto_exposure_time_upper_limit_txt_ctrl = wx.TextCtrl( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		auto_expose_upper_limit_bsizer.Add( self.auto_exposure_time_upper_limit_txt_ctrl, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticText9 = wx.StaticText( self.main_panel, wx.ID_ANY, u"milliseconds", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )
		auto_expose_upper_limit_bsizer.Add( self.m_staticText9, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.RIGHT, 5 )
		
		
		auto_expose_upper_limit_bsizer.AddSpacer( ( 10, 0), 0, 0, 5 )
		
		self.m_button311 = wx.Button( self.main_panel, wx.ID_ANY, u"Get", wx.DefaultPosition, wx.Size( 40,-1 ), wx.BU_EXACTFIT )
		auto_expose_upper_limit_bsizer.Add( self.m_button311, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_button31 = wx.Button( self.main_panel, wx.ID_ANY, u"Set", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		auto_expose_upper_limit_bsizer.Add( self.m_button31, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		auto_expose_upper_limit_bsizer.AddSpacer( ( 10, 0), 0, 0, 5 )
		
		self.txt_ctrl12 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Response:", wx.DefaultPosition, wx.Size( 80,-1 ), 0 )
		self.txt_ctrl12.Wrap( -1 )
		self.txt_ctrl12.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		auto_expose_upper_limit_bsizer.Add( self.txt_ctrl12, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.auto_exposure_time_upper_limit_response_txt_ctrl = wx.StaticText( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.auto_exposure_time_upper_limit_response_txt_ctrl.Wrap( -1 )
		auto_expose_upper_limit_bsizer.Add( self.auto_exposure_time_upper_limit_response_txt_ctrl, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer32.Add( auto_expose_upper_limit_bsizer, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )
		
		auto_expose_upper_limit_bsizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText81 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Auto Exposure Time\n     Lower Limit", wx.DefaultPosition, wx.Size( 180,-1 ), 0 )
		self.m_staticText81.Wrap( -1 )
		self.m_staticText81.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		auto_expose_upper_limit_bsizer1.Add( self.m_staticText81, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.auto_exposure_time_lower_limit_txt_ctrl = wx.TextCtrl( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		auto_expose_upper_limit_bsizer1.Add( self.auto_exposure_time_lower_limit_txt_ctrl, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.m_staticText91 = wx.StaticText( self.main_panel, wx.ID_ANY, u"milliseconds", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText91.Wrap( -1 )
		auto_expose_upper_limit_bsizer1.Add( self.m_staticText91, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5 )
		
		
		auto_expose_upper_limit_bsizer1.AddSpacer( ( 10, 0), 0, 0, 5 )
		
		self.m_button3111 = wx.Button( self.main_panel, wx.ID_ANY, u"Get", wx.DefaultPosition, wx.Size( 40,-1 ), wx.BU_EXACTFIT )
		auto_expose_upper_limit_bsizer1.Add( self.m_button3111, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.m_button312 = wx.Button( self.main_panel, wx.ID_ANY, u"Set", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		auto_expose_upper_limit_bsizer1.Add( self.m_button312, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		
		auto_expose_upper_limit_bsizer1.AddSpacer( ( 10, 0), 0, 0, 5 )
		
		self.txt_ctrl121 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Response:", wx.DefaultPosition, wx.Size( 80,-1 ), 0 )
		self.txt_ctrl121.Wrap( -1 )
		self.txt_ctrl121.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		auto_expose_upper_limit_bsizer1.Add( self.txt_ctrl121, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.auto_exposure_time_lower_limit_response_txt_ctrl = wx.StaticText( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.auto_exposure_time_lower_limit_response_txt_ctrl.Wrap( -1 )
		auto_expose_upper_limit_bsizer1.Add( self.auto_exposure_time_lower_limit_response_txt_ctrl, 0, wx.ALL, 5 )
		
		
		bSizer32.Add( auto_expose_upper_limit_bsizer1, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL|wx.EXPAND, 5 )
		
		auto_expose_upper_limit_bsizer2 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText82 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Auto Exposure Gain\n     Upper Limit", wx.DefaultPosition, wx.Size( 180,-1 ), 0 )
		self.m_staticText82.Wrap( -1 )
		self.m_staticText82.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		auto_expose_upper_limit_bsizer2.Add( self.m_staticText82, 0, wx.ALL, 5 )
		
		self.auto_gain_upper_limit_txt_ctrl = wx.TextCtrl( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		auto_expose_upper_limit_bsizer2.Add( self.auto_gain_upper_limit_txt_ctrl, 0, wx.ALL, 5 )
		
		self.m_staticText92 = wx.StaticText( self.main_panel, wx.ID_ANY, u"milliseconds", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText92.Wrap( -1 )
		auto_expose_upper_limit_bsizer2.Add( self.m_staticText92, 0, wx.BOTTOM|wx.RIGHT|wx.TOP, 5 )
		
		
		auto_expose_upper_limit_bsizer2.AddSpacer( ( 10, 0), 0, 0, 5 )
		
		self.m_button3112 = wx.Button( self.main_panel, wx.ID_ANY, u"Get", wx.DefaultPosition, wx.Size( 40,-1 ), wx.BU_EXACTFIT )
		auto_expose_upper_limit_bsizer2.Add( self.m_button3112, 0, wx.ALL, 5 )
		
		self.m_button313 = wx.Button( self.main_panel, wx.ID_ANY, u"Set", wx.DefaultPosition, wx.Size( 40,-1 ), 0 )
		auto_expose_upper_limit_bsizer2.Add( self.m_button313, 0, wx.ALL, 5 )
		
		
		auto_expose_upper_limit_bsizer2.AddSpacer( ( 10, 0), 0, 0, 5 )
		
		self.txt_ctrl122 = wx.StaticText( self.main_panel, wx.ID_ANY, u"Response:", wx.DefaultPosition, wx.Size( 80,-1 ), 0 )
		self.txt_ctrl122.Wrap( -1 )
		self.txt_ctrl122.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString ) )
		
		auto_expose_upper_limit_bsizer2.Add( self.txt_ctrl122, 0, wx.ALL, 5 )
		
		self.auto_gain_upper_limit_response_txt_ctrl = wx.StaticText( self.main_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.auto_gain_upper_limit_response_txt_ctrl.Wrap( -1 )
		auto_expose_upper_limit_bsizer2.Add( self.auto_gain_upper_limit_response_txt_ctrl, 0, wx.ALL, 5 )
		
		
		bSizer32.Add( auto_expose_upper_limit_bsizer2, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer8.Add( bSizer32, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer44.Add( bSizer8, 1, wx.EXPAND, 5 )
		
		
		self.main_panel.SetSizer( bSizer44 )
		self.main_panel.Layout()
		bSizer44.Fit( self.main_panel )
		main_size.Add( self.main_panel, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.SetSizer( main_size )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_button14211.Bind( wx.EVT_BUTTON, self.ptp_enable )
		self.m_button311.Bind( wx.EVT_BUTTON, self.get_auto_exposure_time_upper_limit )
		self.m_button31.Bind( wx.EVT_BUTTON, self.set_auto_exposure_time_upper_limit )
		self.m_button3111.Bind( wx.EVT_BUTTON, self.get_auto_exposure_time_lower_limit )
		self.m_button312.Bind( wx.EVT_BUTTON, self.set_auto_exposure_time_lower_limit )
		self.m_button3112.Bind( wx.EVT_BUTTON, self.get_auto_gain_upper_limit )
		self.m_button313.Bind( wx.EVT_BUTTON, self.set_auto_gain_upper_limit )
	
	def __del__( self ):
		# Disconnect Events
		self.m_button14211.Unbind( wx.EVT_BUTTON, None )
		self.m_button311.Unbind( wx.EVT_BUTTON, None )
		self.m_button31.Unbind( wx.EVT_BUTTON, None )
		self.m_button3111.Unbind( wx.EVT_BUTTON, None )
		self.m_button312.Unbind( wx.EVT_BUTTON, None )
		self.m_button3112.Unbind( wx.EVT_BUTTON, None )
		self.m_button313.Unbind( wx.EVT_BUTTON, None )
	
	
	# Virtual event handlers, overide them in your derived class
	def ptp_enable( self, event ):
		event.Skip()
	
	def get_auto_exposure_time_upper_limit( self, event ):
		event.Skip()
	
	def set_auto_exposure_time_upper_limit( self, event ):
		event.Skip()
	
	def get_auto_exposure_time_lower_limit( self, event ):
		event.Skip()
	
	def set_auto_exposure_time_lower_limit( self, event ):
		event.Skip()
	
	def get_auto_gain_upper_limit( self, event ):
		event.Skip()
	
	def set_auto_gain_upper_limit( self, event ):
		event.Skip()
	

