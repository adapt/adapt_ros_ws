#!/usr/bin/env python
"""
Library handling imagery simulation.
"""
from __future__ import division, print_function
import wx
from wxpython_gui.system_control_panel.gui import MainFrame
import rospy

def main():
    node_name = 'system_gui'
    name_space = rospy.get_namespace()

    topic_names = {}

    # --------------------- Remote Image Server Topics -----------------------
    topic_names['rgb_image_topic'] = '/image_raw'
    topic_names['rgb_analytics_image_topic'] = '/rgb/segmented'

    topic_names['rgb_genicam_get_service_topic'] = '/rc_genicam_camera/get_genicam_parameter'
    topic_names['rgb_genicam_set_service_topic'] = '/rc_genicam_camera/set_genicam_parameter'
    # ------------------------------------------------------------------------

    # ------------------------ Archive Service Topics ------------------------
    topic_names['archive_srv'] = '/set_archiving'
    # ------------------------------------------------------------------------

    # ------------------------ Exposure Value Topics -------------------------
    topic_names['rgb_exposure'] = '/rgb_driver/exposure'
    # ------------------------------------------------------------------------

    # --------------------------- DiagnosticStatus ---------------------------
    topic_names['ptp_health_ins'] = '/ptp_health/ins'
    topic_names['ptp_health_nmea'] = '/ptp_health/nmea'
    topic_names['ptp_health_pps'] = '/ptp_health/pps'
    topic_names['ptp_health_ptp'] = '/ptp_health/ptp'
    # ------------------------------------------------------------------------

    # --------------------------- Disk Space Topics --------------------------
    topic_names['disk_space'] = '/disk_free_bytes'
    # ------------------------------------------------------------------------

    topic_names['det_topic1'] = ''.join([name_space,
                                         'sprokit_detector_adapter/detections_out'])

    topic_names['nav_sat_fix_topic'] = '/an_device/NavSatFix'
    topic_names['imu_fix_topic'] = '/an_device/Imu'
    topic_names['nav_filter_status_topic'] = '/an_device/FilterStatus'

    window_title = 'System Control Panel'

    app = wx.App(False)
    frame = MainFrame(node_name, topic_names, False, window_title)
    frame.Show(True)
    app.MainLoop()

if __name__ == '__main__':
    main()
